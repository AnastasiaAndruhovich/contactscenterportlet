package by.andruhovich.contacts.portlet;

import by.andruhovich.contacts.constants.ContactsCenterPortletKeys;
import by.andruhovich.contacts.model.CustomUser;
import by.andruhovich.contacts.parser.UserJSONParser;
import by.andruhovich.contacts.service.CustomUserService;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

import static by.andruhovich.contacts.constants.ContactsCenterWebKeys.*;

/**
 * @author AndruhovichA
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ContactsCenterPortletKeys.ContactsCenter,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ContactsCenterPortlet extends MVCPortlet {
	@Reference
	private UserLocalService userLocalService;

	@Reference
	private CustomUserService customUserService;

	private static final int START_PAGE = 1;

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		int endIndex = USER_COUNT_PER_PAGE + 1;
		List<CustomUser> customUsers = customUserService.getUserList(START_PAGE, endIndex);

		int userTotalCount = userLocalService.getUsersCount();
		int pageCount = calculatePageCount(userTotalCount);

		ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String ajaxURL = renderResponse.createResourceURL().toString();
		JSONObject userData = createUserData(customUsers, pageCount, ajaxURL, themeDisplay);

		renderRequest.setAttribute(JSON_USER_DATA, userData);

		super.doView(renderRequest, renderResponse);
	}

	private JSONObject createUserData(List<CustomUser> customUsers, int pageCount, String ajaxURL, ThemeDisplay themeDisplay) {
		JSONObject userData = JSONFactoryUtil.createJSONObject();

		UserJSONParser userJSONParser = new UserJSONParser();
		JSONArray userJSONList = userJSONParser.parseCustomUserList(customUsers);

		userData.put(JSON_USER_LIST, userJSONList);
		userData.put(JSON_ACTIVE_PAGE, START_PAGE);
		userData.put(JSON_COUNT_PER_PAGE, USER_COUNT_PER_PAGE);
		userData.put(JSON_TOTAL_COUNT, pageCount);
		userData.put(JSON_AJAX_URL, ajaxURL);
		userData.put(JSON_LANGUAGE, createLanguageJSONBundle(getResourceBundle(themeDisplay.getLocale())));

		return userData;
	}

	private JSONObject createLanguageJSONBundle(ResourceBundle resourceBundle) {
		JSONObject languageResourceBundle = JSONFactoryUtil.createJSONObject();
		resourceBundle.keySet().forEach(key -> languageResourceBundle.put(key, resourceBundle.getString(key)));
		return languageResourceBundle;
	}

	private int calculatePageCount(int userTotalCount) {
		int pageCount = userTotalCount / USER_COUNT_PER_PAGE;
		int rest = userTotalCount % USER_COUNT_PER_PAGE;
		if (rest != 0) {
			pageCount++;
		}
		return pageCount;
	}



}