package by.andruhovich.contacts.portlet.resources;

import by.andruhovich.contacts.constants.ContactsCenterPortletKeys;
import by.andruhovich.contacts.model.CustomUser;
import by.andruhovich.contacts.parser.UserJSONParser;
import by.andruhovich.contacts.service.CustomUserService;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.PortalUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static by.andruhovich.contacts.constants.ContactsCenterWebKeys.REQUEST_ACTIVE_PAGE;
import static by.andruhovich.contacts.constants.ContactsCenterWebKeys.USER_COUNT_PER_PAGE;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + ContactsCenterPortletKeys.ContactsCenter,
                "mvc.command.name=getUserList"
        },
        service = MVCResourceCommand.class
)
public class UserResourceCommand implements MVCResourceCommand {
    @Reference
    private CustomUserService customUserService;

    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        int activePage = getActivePageParameter(resourceRequest);
        int startIndex = (activePage - 1) * USER_COUNT_PER_PAGE + 1;

        List<CustomUser> customUsers = customUserService.getUserList(startIndex, USER_COUNT_PER_PAGE);
        UserJSONParser userJSONParser = new UserJSONParser();
        JSONArray userJSONList = userJSONParser.parseCustomUserList(customUsers);

        try {
            resourceResponse.getWriter().print(userJSONList.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private int getActivePageParameter(ResourceRequest resourceRequest) {
        HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));
        String page = httpReq.getParameter(REQUEST_ACTIVE_PAGE);


        int activePage;
        if (page == null) {
            activePage = 1;
        } else {
            activePage = Integer.parseInt(page);
        }
        return activePage;
    }
}
