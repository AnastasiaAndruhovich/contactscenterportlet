package by.andruhovich.contacts.portlet.resources;

import by.andruhovich.contacts.constants.ContactsCenterPortletKeys;
import by.andruhovich.contacts.model.CustomUser;
import by.andruhovich.contacts.parser.UserJSONParser;
import by.andruhovich.contacts.service.CustomUserService;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;

import static by.andruhovich.contacts.constants.ContactsCenterWebKeys.REQUEST_USER_ID;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + ContactsCenterPortletKeys.ContactsCenter,
                "mvc.command.name=getUserInfo"
        },
        service = MVCResourceCommand.class
)
public class UserInfoResourceCommand implements MVCResourceCommand {
    @Reference
    private CustomUserService customUserService;

    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        String id = ParamUtil.getString(resourceRequest, REQUEST_USER_ID);
        Long userId = Long.parseLong(id);
        CustomUser customUser = customUserService.findByUserId(userId);

        UserJSONParser userJSONParser = new UserJSONParser();
        JSONObject userJSON = userJSONParser.parseCustomUser(customUser);

        try {
            resourceResponse.getWriter().print(userJSON.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
