package by.andruhovich.contacts.portlet.resources;

import by.andruhovich.contacts.constants.ContactsCenterPortletKeys;
import by.andruhovich.contacts.model.CustomUser;
import by.andruhovich.contacts.parser.UserJSONParser;
import by.andruhovich.contacts.service.CustomUserService;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.PortalUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static by.andruhovich.contacts.constants.ContactsCenterWebKeys.REQUEST_NAME;

@Component(
    immediate = true,
    property = {
            "javax.portlet.name=" + ContactsCenterPortletKeys.ContactsCenter,
            "mvc.command.name=searchUser"
    },
    service = MVCResourceCommand.class
)
public class SearchUserResourceCommand implements MVCResourceCommand {
    @Reference
    private CustomUserService customUserService;

    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));
        String name = httpReq.getParameter(REQUEST_NAME);

        List<CustomUser> customUsers = new LinkedList<>();
        customUsers.addAll(customUserService.findByFirstName(name));
        customUsers.addAll(customUserService.findByLastName(name));

        UserJSONParser userJSONParser = new UserJSONParser();
        JSONArray userJSONList = userJSONParser.parseCustomUserList(customUsers);

        try {
            resourceResponse.getWriter().print(userJSONList.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


}
