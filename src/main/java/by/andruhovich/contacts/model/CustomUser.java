package by.andruhovich.contacts.model;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserWrapper;

import java.util.Date;

public class CustomUser extends UserWrapper {
    private String workingPlace;
    private Integer divisionCode;
    private String telephoneNumber;

    private String branchName;
    private String divisionName;
    private String mobilePhone;
    private Date hireDate;
    private Date fireDate;

    public CustomUser(User user, String workingPlace, Integer divisionCode, String telephoneNumber, String branchName,
                      String divisionName, String mobilePhone, Date hireDate, Date fireDate) {
        super(user);
        this.workingPlace = workingPlace;
        this.divisionCode = divisionCode;
        this.telephoneNumber = telephoneNumber;
        this.branchName = branchName;
        this.divisionName = divisionName;
        this.mobilePhone = mobilePhone;
        this.hireDate = hireDate;
        this.fireDate = fireDate;
    }

    public String getWorkingPlace() {
        return workingPlace;
    }

    public void setWorkingPlace(String workingPlace) {
        this.workingPlace = workingPlace;
    }

    public Integer getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(Integer divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Date getFireDate() {
        return fireDate;
    }

    public void setFireDate(Date fireDate) {
        this.fireDate = fireDate;
    }
}
