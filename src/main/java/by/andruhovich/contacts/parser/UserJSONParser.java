package by.andruhovich.contacts.parser;

import by.andruhovich.contacts.model.CustomUser;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Image;
import com.liferay.portal.kernel.service.ImageLocalServiceUtil;

import java.util.Base64;
import java.util.List;

public class UserJSONParser {
    private static final String USER_ID = "userId";
    private static final String EMAIL = "emailAddress";
    private static final String SCREEN_NAME = "screenName";
    private static final String FULL_NAME = "fullName";
    private static final String WORKING_PLACE = "workingPlace";
    private static final String DIVISION_CODE = "divisionCode";
    private static final String JOB_TITLE = "jobTitle";
    private static final String TELEPHONE_NUMBER = "telephoneNumber";
    private static final String BRANCH_NAME = "branchName";
    private static final String DIVISION_NAME = "divisionName";
    private static final String MOBILE_PHONE = "mobilePhone";
    private static final String HIRE_DATE = "hireDate";
    private static final String FIRE_DATE = "fireDate";
    private static final String IMAGE = "image";

    public JSONArray parseCustomUserList(List<CustomUser> customUsers) {
        JSONArray userJSONList = JSONFactoryUtil.createJSONArray();
        for (CustomUser customUser : customUsers) {
            JSONObject userJSON = parseCustomUser(customUser);
            userJSONList.put(userJSON);
        }
        return userJSONList;
    }

    public JSONObject parseCustomUser(CustomUser customUser) {
        JSONObject userJSON = JSONFactoryUtil.createJSONObject();
        userJSON.put(USER_ID, customUser.getUserId());
        userJSON.put(EMAIL, customUser.getEmailAddresses());
        userJSON.put(SCREEN_NAME, customUser.getScreenName());
        userJSON.put(FULL_NAME, customUser.getFullName());
        userJSON.put(WORKING_PLACE, customUser.getWorkingPlace());
        userJSON.put(DIVISION_CODE, customUser.getDivisionCode());
        userJSON.put(JOB_TITLE, customUser.getJobTitle());
        userJSON.put(TELEPHONE_NUMBER, customUser.getTelephoneNumber());
        userJSON.put(BRANCH_NAME, customUser.getBranchName());
        userJSON.put(DIVISION_NAME, customUser.getDivisionName());
        userJSON.put(MOBILE_PHONE, customUser.getMobilePhone());
        userJSON.put(HIRE_DATE, customUser.getHireDate());
        userJSON.put(FIRE_DATE, customUser.getFireDate());
        String imgSource = convertUserImage(customUser);
        userJSON.put(IMAGE, imgSource);
        return userJSON;
    }

    private String convertUserImage(CustomUser customUser) {
        long portraitId = customUser.getPortraitId();
        String imgSource = null;
        try {
            Image image = ImageLocalServiceUtil.getImage(portraitId);
            if (image != null) {
                byte[] array = image.getTextObj();
                imgSource = new String(Base64.getEncoder().encode(array));
            }
        } catch (PortalException e) {
            imgSource = null;
        }
        return imgSource;
    }
}
