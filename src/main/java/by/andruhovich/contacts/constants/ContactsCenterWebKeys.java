package by.andruhovich.contacts.constants;

/**
 * @author AndruhovichA
 */
public class ContactsCenterWebKeys {
	public static final String JSON_ACTIVE_PAGE = "activepage";
	public static final String JSON_COUNT_PER_PAGE = "countperpage";
	public static final String JSON_TOTAL_COUNT = "totalcount";
	public static final String JSON_USER_LIST = "userList";
	public static final String JSON_USER_DATA = "userData";
	public static final String JSON_LANGUAGE = "language";
	public static final String JSON_AJAX_URL = "ajaxURL";

	public static final int USER_COUNT_PER_PAGE = 10;

	public static final String REQUEST_NAME = "name";
	public static final String REQUEST_ACTIVE_PAGE = "activePage";
	public static final String REQUEST_USER_ID = "userId";

}