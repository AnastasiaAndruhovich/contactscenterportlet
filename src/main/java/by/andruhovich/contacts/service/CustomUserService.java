package by.andruhovich.contacts.service;

import by.andruhovich.contacts.model.CustomUser;
import com.liferay.portal.kernel.model.User;

import java.util.List;

public interface CustomUserService {
    List<CustomUser> findByFirstName(String firstName);
    List<CustomUser> findByLastName(String lastName);
    CustomUser findByUserId(long userId);

    List<CustomUser> getUserList(int startIndex, int count);

    List<CustomUser> convertToCustomUsers(List<User> users);
    CustomUser convertToCustomUser(User user);
}
