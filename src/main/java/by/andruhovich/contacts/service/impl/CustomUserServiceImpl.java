package by.andruhovich.contacts.service.impl;

import by.andruhovich.contacts.model.CustomUser;
import by.andruhovich.contacts.service.CustomUserService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Component(
        immediate = true,
        service = CustomUserService.class
)
public class CustomUserServiceImpl implements CustomUserService {
    @Reference
    private UserLocalService userLocalService;

    private static final String FIRST_NAME_FIELD = "firstName";
    private static final String LAST_NAME_FIELD = "lastName";

    private static final Log log = LogFactoryUtil.getLog(CustomUserService.class);

    @Override
    public List<CustomUser> findByFirstName(String firstName) {
        return findCustomUserByField(FIRST_NAME_FIELD, firstName);
    }

    @Override
    public List<CustomUser> findByLastName(String lastName) {
        return findCustomUserByField(LAST_NAME_FIELD, lastName);
    }

    @Override
    public CustomUser findByUserId(long userId) {
        CustomUser customUser = null;

        try {
            User user = userLocalService.getUser(userId);
            customUser = convertToCustomUser(user);
        } catch (PortalException e) {
            log.error(e.getMessage());
        }
        return customUser;
    }

    @Override
    public List<CustomUser> getUserList(int startIndex, int count) {
        List<CustomUser> customUsers;

        List<User> users = userLocalService.getUsers(startIndex, startIndex + count);
        customUsers = convertToCustomUsers(users);
        return customUsers;
    }

    @Override
    public List<CustomUser> convertToCustomUsers(List<User> users) {
        List<CustomUser> customUserList = new LinkedList<>();

        for (User user : users) {
            CustomUser customUser = convertToCustomUser(user);
            customUserList.add(customUser);
        }
        return customUserList;
    }

    @Override
    public CustomUser convertToCustomUser(User user) {
        final String WORKING_PLACE = "workingPlace";
        final String DIVISION_CODE = "divisionCode";
        final String TELEPHONE_NUMBER = "telephoneNumber";
        final String BRANCH_NAME = "branchName";
        final String DIVISION_NAME = "divisionName";
        final String MOBILE_PHONE = "mobilePhone";
        final String HIRE_DATE = "hireDate";
        final String FIRE_DATE = "fireDate";

        String workingPlace = (String) user.getExpandoBridge().getAttribute(WORKING_PLACE, false);
        Integer divisionCode = (Integer) user.getExpandoBridge().getAttribute(DIVISION_CODE, false);
        String telephoneNumber = (String) user.getExpandoBridge().getAttribute(TELEPHONE_NUMBER, false);
        String branchName = (String) user.getExpandoBridge().getAttribute(BRANCH_NAME, false);
        String divisionName = (String) user.getExpandoBridge().getAttribute(DIVISION_NAME, false);
        String mobilePhone = (String) user.getExpandoBridge().getAttribute(MOBILE_PHONE, false);
        Date hireDate = (Date) user.getExpandoBridge().getAttribute(HIRE_DATE, false);
        Date fireDate = (Date) user.getExpandoBridge().getAttribute(FIRE_DATE, false);

        return new CustomUser(user, workingPlace, divisionCode, telephoneNumber, branchName,
                divisionName, mobilePhone, hireDate, fireDate);
    }

    private List<CustomUser> findCustomUserByField(String fieldName, String fieldValue) {
        final String CREATE_DATE = "createDate";

        List<CustomUser> customUsers = new LinkedList<>();

        try {
            DynamicQuery query = userLocalService.dynamicQuery();
            query.add(PropertyFactoryUtil.forName(fieldName).eq(fieldValue));
            query.addOrder(OrderFactoryUtil.desc(CREATE_DATE));

            List<User>  users = userLocalService.dynamicQuery(query);
            customUsers = convertToCustomUsers(users);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return customUsers;
    }
}
