import React from 'react'

const UserCardInfo = ({users, user, onSearchUserInfo, returnToUserList, language}) => (
    <div className="row personInfo">
        <div className="col-md-3">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>{language.table_full_name}</th>
                </tr>
                </thead>
                <tbody>
                {
                    users.map(userItem => (
                            <tr key={userItem.userId}>
                                <th scope="row">
                                    <a onClick={()=>{onSearchUserInfo(userItem)}}>{userItem.fullName}</a>
                                </th>
                            </tr>
                        )
                    )
                }
                </tbody>
            </table>
        </div>

        <div className="col-md-9">
            <div className="row">
                <div className="col-sm-3">
                    {user.image == null ?
                        <img className="img-responsive pimage" src="/o/contactsCenterPortlet/img/user_male_portrait.jpg"/> :
                        <img className="img-responsive pimage" src={`data:image/gif;base64,${user.image}`}/>}
                </div>
                <div className="col-sm-9">
                    <div className="row">
                        <div className="col-sm-10">
                            <h4 className="bold" style={{paddingLeft: '8px'}}>{user.fullName}</h4>
                        </div>
                        <div className="col-sm-2">
                            <a onClick={()=>{returnToUserList()}}>
                                <span className="icon-monospaced fr mt8" id="uwrv____">
                                    <svg className="lexicon-icon lexicon-icon-angle-left" focusable="false" role="img" title="" viewBox="0 0 512 512">
                                        <title>angle-left</title>
                                        <path className="lexicon-icon-outline" d="M114.106 254.607c0.22 6.936 2.972 13.811 8.272 19.11l227.222 227.221c11.026 11.058 28.94 11.058 39.999 0 11.058-11.026 11.058-28.94 0-39.999l-206.333-206.333c0 0 206.333-206.333 206.333-206.333 11.058-11.059 11.058-28.973 0-39.999-11.058-11.059-28.973-11.059-39.999 0l-227.221 227.221c-5.3 5.3-8.052 12.174-8.273 19.111z"/>
                                     </svg>
                                </span>
                            </a>
                        </div>
                    </div>
                    <table className="table table-striped">
                        <thead>
                        <tr/>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{language.user_branch_name}</td>
                            <th>{user.branchName}</th>
                        </tr>
                        <tr>
                            <td>{language.user_division_name}</td>
                            <th>{user.divisionName}</th>
                        </tr>
                        <tr>
                            <td>{language.user_position}</td>
                            <th>{user.jobTitle}</th>
                        </tr>
                        <tr>
                            <td>{language.user_telephone_number}</td>
                            <th>{user.telephoneNumber}</th>
                        </tr>
                        <tr>
                            <td>{language.user_mobile_number}</td>
                            <th>{user.mobilePhone}</th>
                        </tr>
                        <tr>
                            <td>{language.user_working_place}</td>
                            <th>{user.workingPlace}</th>
                        </tr>
                        <tr>
                            <td>{language.user_email}</td>
                            <th>{user.emailAddress}</th>
                        </tr>
                        <tr>
                            <td>{language.user_hire_date}</td>
                            <th>{user.hireDate}</th>
                        </tr>
                        <tr>
                            <td>{language.user_fire_date}</td>
                            <th>{user.fireDate}</th>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
);

export default UserCardInfo