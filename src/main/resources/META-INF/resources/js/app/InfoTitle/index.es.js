import React from 'react'

const InfoTitle = ({title}) => (
    <div><p>{title}</p></div>
);

export default InfoTitle