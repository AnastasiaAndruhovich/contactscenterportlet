import React from 'react'

const Users = ({users, onSearchUserInfo, language}) => (
    <div className="table-responsive">
        <table className="table table-striped">
            <thead>
            <tr>
                <th>{language.table_full_name}</th>
                <th>{language.table_email}</th>
                <th>{language.table_working_place}</th>
                <th>{language.table_division_code}</th>
                <th>{language.table_position}</th>
                <th>{language.table_telephone_number}</th>
            </tr>
            </thead>
            <tbody>
            {
                users.map( user => (
                    <tr key={user.userId}>
                        <th scope="row">
                            <a onClick={()=>{onSearchUserInfo(user)}}>{user.fullName}</a>
                        </th>
                        <td>{user.emailAddress}</td>
                        <td>{user.workingPlace}</td>
                        <td>{user.divisionCode}</td>
                        <td>{user.jobTitle}</td>
                        <td>{user.telephoneNumber}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    </div>
);

export default Users
