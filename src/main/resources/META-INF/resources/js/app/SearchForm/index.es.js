import React from 'react'


const SearchPersonForm = ({name, handleNameChange, handleSubmitSearch, language}) => (
    <div>
        <div className="col-md-4">
            <div className="form-group basic-search input-group">
                <div className="input-group-input">
                    <div className="basic-search-slider">
                        <input type="text" className="form-control search-query" value={name} placeholder={language.placeholder_name} onChange={handleNameChange}/>
                    </div>
                </div>
                <div className="input-group-btn">
                    <button type="button" className="btn btn-default" onClick={handleSubmitSearch}>
                        <span className="glyphicon glyphicon-search"/>
                    </button>
                </div>
            </div>
        </div>

    </div>

);

export default SearchPersonForm