import React from 'react'
import Pager from 'react-pager-component';

import SearchPersonForm from './SearchForm/index.es'
import InfoTitle from './InfoTitle/index.es'
import Users from './Users/index.es'
import UserCardInfo from './Profile/index.es'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: props.userData.userList,
            activePage: props.userData.activepage,
            totalCount: props.userData.totalcount,
            language: props.userData.language,

            name: '',
            infoTitle: '',
            user: '',
            isSearching: false
        };
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.onSearchUserInfo = this.onSearchUserInfo.bind(this);
        this.returnToUserList = this.returnToUserList.bind(this);
    }

    handlePageChange (activePage) {
        fetch(`${this.props.userData.ajaxURL}&p_p_resource_id=getUserList&activePage=${activePage}`, {
            method: 'GET',
            mode: 'cors',
        })
            .then(response => {
                response.json().then(users => {
                    this.setState({users,
                        activePage: activePage
                    });
                })
            })
            .catch(error => {
               console.log(error);
            })
    };

    handleNameChange(event) {
        this.setState({
            name: event.target.value
        })
    }

    handleSearch(event) {
        this.setState({
            isSearching: true,
        });

        fetch(`${this.props.userData.ajaxURL}&p_p_resource_id=searchUser&name=${this.state.name}`, {
            method: 'GET',
            mode: 'cors',
        })
            .then(response => {
                response.json().then(users => {
                    users.length > 0 ?
                        this.setState({users,
                            infoTitle: `По запросу "${this.state.name}" найдено ${users.length} сотрудников`,
                            name: ''
                        }) :
                        this.setState({users,
                            infoTitle: `По запросу "${this.state.name}" не найдено ни одного сотрудника.`,
                            name: '',
                        })
                })
            })
            .catch(error => {
                this.setState( {
                    infoTitle: `По запросу "${this.state.name}" не найдено ни одного сотрудника.`
                })
            })
    }

    returnToUserList() {
        this.setState({
            user: '',
            infoTitle: '',
            name: '',
            isSearching: false
        });
        this.handlePageChange(this.state.activePage);
    }

    onSearchUserInfo(user) {
        const formData = new FormData();
        formData.append(`${this.props.namespace}userId`, user.userId);

        fetch(`${this.props.userData.ajaxURL}&p_p_resource_id=getUserInfo`, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            body : formData
        })
            .then(response => {
                response.json().then(user => {
                    this.setState({user})
                })
            })
            .catch(error => {
                console.log("error")
            })
    }

    render() {
        const {users, activePage, totalCount, language, name, infoTitle, user, isSearching} = this.state;
        return (
            <div>
                <SearchPersonForm name={name} language={language} handleNameChange={this.handleNameChange} handleSubmitSearch={this.handleSearch}/>
                <InfoTitle title={infoTitle}/>
                {user ?
                    <UserCardInfo
                        users={users}
                        user={user}
                        returnToUserList={this.returnToUserList}
                        onSearchUserInfo={this.onSearchUserInfo}
                        language={language}/> :
                    <Users users={users} onSearchUserInfo={this.onSearchUserInfo} language={language}/>
                }
                {!isSearching ?
                    <Pager
                        length={totalCount}
                        current={activePage}
                        onChange={this.handlePageChange}
                        showRestLabels={false}
                        lastButtonLabel={language.pager_last}
                        firstButtonLabel={language.pager_first}/> : null
                }
            </div>
        )
    }
}

export default App
