import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/index.es'

export default function(elementId, userData) {
    ReactDOM.render(<App userData={userData} namespace={elementId}/>, document.getElementById(elementId));
}
