<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<%@ include file="/init.jsp" %>

<div id="<portlet:namespace />"></div>

<aui:script require="contactsCenterPortlet@1.0.0">
	contactsCenterPortlet100.default('<portlet:namespace />', ${userData});
</aui:script>